# Code Overview -- Mark Cutler

---

Below, is a quick overview of the major packages I have written (or mostly written) and, to varying extents, maintain for [my lab](http://acl.mit.edu).

- [**acl-utils**](https://bitbucket.org/acl-mit/acl-utils) -- Two separate libraries (c++ and python) containing commonly called functions, such as communication code (serial, udp, and bluetooth), dynamics models, etc.  I also wrote an install script that checks for and, if needed, installs dependencies before installing the libraries.
- [**acl-ros-pkg**](https://bitbucket.org/acl-mit/acl-ros-pkg) -- A collection of ROS nodes that make up the bulk of the control and simulation code we use.  Includes simulations and real-time controllers for quadrotors and ground robots.
- [**vicon-filters**](https://bitbucket.org/acl-mit/vicon-filters) -- Several real-time filters for extracting robust position, velocity, attitude, and angular rate data from a Vicon motion capture system.
- [**raven_rviz**](https://bitbucket.org/acl-mit/raven_rviz) -- Visualization code based on the RVIZ project.  Includes the ability to dynamically add and subtract vehicles based on the current data stream from either a motion capture source or a simulator.
- [**UberPilot-mc**](https://bitbucket.org/acl-mit/uberpilot-mc) -- On-board code for the UberPilot versions < 2.0.  The autopilot is based on a microchip 16 bit microcontroller.  This is the code currently used by the ACL for all flying vehicles.
- [**UberPilot-ti**](https://bitbucket.org/acl-mit/uberpilot-ti) -- On-board code for the UberPilot versions >= 2.0.  The autopilot is based on a TI 32 bit, dual-core microcontroller.  This code is experimental at this point.
- [**UberPilot-pcb**](https://bitbucket.org/markjcutler/uberpilot-pcb) -- Eagle schematics and design files for the UberPilots.

---